
function flatten(arr,depth=1,res=[]){
     // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    
  
    if(Array.isArray(arr)){
            
    for (let eachIndex=0;eachIndex<arr.length;eachIndex++){

        if(Array.isArray(arr[eachIndex]) && depth!==0){
            flatten(arr[eachIndex],depth-1,res)

        }
        else{
            if(arr[eachIndex]!==undefined){
                res.push(arr[eachIndex])

            }
            
        }
    }
    }

    else{
        
        return []
    }

    return res

    
}

module.exports=flatten

