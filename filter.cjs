function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
   // console.log(elements,cb)

   /*• The filter function accepts truthy values from the 
   
   callback function but at MountBlue we want you to use strict 
   checks to minimize potential bugs later. Modify your filter 
   logic to only work when there is true returned strictly.
   
   All other truthy values can be discarded.*/

   
    if(Array.isArray(elements) && typeof(cb)==="function"){
        let elementsArr=[]
        for (let index=0;index<elements.length;index++){
            let checkTruthy=cb(elements[index],index,elements)
           if(checkTruthy==true){
            
            elementsArr.push(elements[index])
           }
           }
           return elementsArr
    return []
    
   }

}

module.exports=filter