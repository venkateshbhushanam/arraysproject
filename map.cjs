function map(array, cb) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element 
    //in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.

    



    let res = []
    if (Array.isArray(array) && typeof (cb) === "function" && typeof (cb) != 'undefined') {
        for (let eachIndex = 0; eachIndex < array.length; eachIndex++) {
            res.push(cb(array[eachIndex], eachIndex, array))
        }
        return res
    }
    else {
        return
    }
}

module.exports = map
